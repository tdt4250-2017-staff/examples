package tdt4250.hal.univ.tests;

import static org.junit.Assert.assertEquals;

import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.ecore.util.Diagnostician;
import org.junit.Test;

import tdt4250.hal.univ.Person;
import tdt4250.hal.univ.UnivFactory;

public class UnivValidatorTest {

	@Test
	public void testShortNameConstraintNotViolated() {
		Person person = UnivFactory.eINSTANCE.createPerson();
		person.setName("Hallvard Trætteberg");
		person.setShortName("hal");
		Diagnostic diagnostics = Diagnostician.INSTANCE.validate(person);
		assertEquals(Diagnostic.OK, diagnostics.getSeverity());
	}
	
	@Test
	public void testShortNameConstraintViolated() {
		Person person = UnivFactory.eINSTANCE.createPerson();
		person.setName("hal");
		person.setShortName("Hallvard Trætteberg");
		Diagnostic diagnostics = Diagnostician.INSTANCE.validate(person);
		assertEquals(Diagnostic.ERROR, diagnostics.getSeverity());
	}
}
