/**
 */
package tdt4250.hal.univ.tests;

import java.io.IOException;

import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.common.util.URI;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;

import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;
import tdt4250.hal.univ.UnivPackage;

/**
 * <!-- begin-user-doc -->
 * A sample utility for the '<em><b>univ</b></em>' package.
 * <!-- end-user-doc -->
 * @generated
 */
public class UnivExample {
	/**
	 * <!-- begin-user-doc -->
	 * Load all the argument file paths or URIs as instances of the model.
	 * <!-- end-user-doc -->
	 * @param args the file paths or URIs.
	 * @generated NOT
	 */
	public static void main(String[] args) {
		// Create a resource set to hold the resources.
		//
		ResourceSet resourceSet = new ResourceSetImpl();
		
		// Register the appropriate resource factory to handle all file extensions.
		//
		resourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap().put
			(Resource.Factory.Registry.DEFAULT_EXTENSION, 
			 new XMIResourceFactoryImpl());

		// Register the package to ensure it is available during loading.
		//
		resourceSet.getPackageRegistry().put
			(UnivPackage.eNS_URI, 
			 UnivPackage.eINSTANCE);
        
		// If there are no arguments, emit an appropriate usage message.
		//
		try {
			Resource resource = resourceSet.createResource(URI.createFileURI("/Users/hal/java/git/tdt4250-2017-staff/examples/tdt4250.hal.myfirstmodel/model/NTNU.xmi"));
			resource.load(null);
			TreeIterator<EObject> it = resource.getAllContents();
			while (it.hasNext()) {
				System.out.println(resourceSet.getResources().size());
				EObject eObject = it.next();
				System.out.println(eObject);
				if (UnivPackage.eINSTANCE.getPerson().isInstance(eObject)) {
					EList<EStructuralFeature> features = eObject.eClass().getEAllStructuralFeatures();
					for (EStructuralFeature feature : features) {
						System.out.println(feature.getName() + ": " + eObject.eGet(feature));
					}
				}
			}
		}
		catch (IOException exception) {
			exception.printStackTrace();
		}
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * Prints diagnostics with indentation.
	 * <!-- end-user-doc -->
	 * @param diagnostic the diagnostic to print.
	 * @param indent the indentation for printing.
	 * @generated
	 */
	protected static void printDiagnostic(Diagnostic diagnostic, String indent) {
		System.out.print(indent);
		System.out.println(diagnostic.getMessage());
		for (Diagnostic child : diagnostic.getChildren()) {
			printDiagnostic(child, indent + "  ");
		}
	}

} //UnivExample
