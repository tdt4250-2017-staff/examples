/**
 */
package tdt4250.hal.univ.tests;

import java.util.Arrays;

import org.eclipse.emf.common.util.EList;

import junit.textui.TestRunner;
import tdt4250.hal.univ.Faculty;
import tdt4250.hal.univ.Person;
import tdt4250.hal.univ.UnivFactory;
import tdt4250.hal.univ.University;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>University</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are tested:
 * <ul>
 *   <li>{@link tdt4250.hal.univ.University#getAllEmployees() <em>All Employees</em>}</li>
 * </ul>
 * </p>
 * <p>
 * The following operations are tested:
 * <ul>
 *   <li>{@link tdt4250.hal.univ.University#isEmployed(tdt4250.hal.univ.Person) <em>Is Employed</em>}</li>
 * </ul>
 * </p>
 * @generated
 */
public class UniversityTest extends NamedTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(UniversityTest.class);
	}

	/**
	 * Constructs a new University test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UniversityTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this University test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected University getFixture() {
		return (University)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(UnivFactory.eINSTANCE.createUniversity());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

	/**
	 * Tests the '{@link tdt4250.hal.univ.University#getAllEmployees() <em>All Employees</em>}' feature getter.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see tdt4250.hal.univ.University#getAllEmployees()
	 * @generated NOT
	 */
	public void testGetAllEmployees() {
		University univ = UnivFactory.eINSTANCE.createUniversity();
		Faculty faculty1 = UnivFactory.eINSTANCE.createFaculty();
		Faculty faculty2 = UnivFactory.eINSTANCE.createFaculty();
		univ.getFaculties().add(faculty1);
		univ.getFaculties().add(faculty2);
		
		Person p1 = UnivFactory.eINSTANCE.createPerson();	
		Person p2 = UnivFactory.eINSTANCE.createPerson();	
		Person p3 = UnivFactory.eINSTANCE.createPerson();	
		faculty1.getStaff().add(p1);
		faculty1.getStaff().add(p2);
		faculty2.getStaff().add(p3);
		
		EList<Person> allEmployees = univ.getAllEmployees();
		assertEquals(faculty1.getStaff().size() + faculty2.getStaff().size(), allEmployees.size());
		assertTrue(allEmployees.containsAll(faculty1.getStaff()));
		assertTrue(allEmployees.containsAll(faculty2.getStaff()));
	}

	/**
	 * Tests the '{@link tdt4250.hal.univ.University#isEmployed(tdt4250.hal.univ.Person) <em>Is Employed</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see tdt4250.hal.univ.University#isEmployed(tdt4250.hal.univ.Person)
	 * @generated NOT
	 */
	public void testIsEmployed__Person_1() {
		University univ = UnivFactory.eINSTANCE.createUniversity();
		Faculty faculty1 = UnivFactory.eINSTANCE.createFaculty(), faculty2 = UnivFactory.eINSTANCE.createFaculty();
		univ.getFaculties().addAll(Arrays.asList(faculty1, faculty2));
		
		Person p1 = UnivFactory.eINSTANCE.createPerson(), p2 = UnivFactory.eINSTANCE.createPerson(), p3 = UnivFactory.eINSTANCE.createPerson(), p4 = UnivFactory.eINSTANCE.createPerson();
		faculty1.getStaff().addAll(Arrays.asList(p1, p2));
		faculty2.getStaff().add(p3);

		assertTrue(univ.isEmployed(p1));
		assertTrue(univ.isEmployed(p2));
		assertTrue(univ.isEmployed(p3));
		assertFalse(univ.isEmployed(p4));
	}

} //UniversityTest
