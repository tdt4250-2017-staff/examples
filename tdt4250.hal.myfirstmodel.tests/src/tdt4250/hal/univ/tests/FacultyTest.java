/**
 */
package tdt4250.hal.univ.tests;

import junit.textui.TestRunner;

import tdt4250.hal.univ.Faculty;
import tdt4250.hal.univ.UnivFactory;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Faculty</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class FacultyTest extends NamedTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(FacultyTest.class);
	}

	/**
	 * Constructs a new Faculty test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FacultyTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Faculty test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected Faculty getFixture() {
		return (Faculty)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(UnivFactory.eINSTANCE.createFaculty());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}
	
} //FacultyTest
