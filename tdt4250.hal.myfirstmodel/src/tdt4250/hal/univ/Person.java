/**
 */
package tdt4250.hal.univ;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Person</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link tdt4250.hal.univ.Person#getAffiliation <em>Affiliation</em>}</li>
 *   <li>{@link tdt4250.hal.univ.Person#getFaculty <em>Faculty</em>}</li>
 * </ul>
 *
 * @see tdt4250.hal.univ.UnivPackage#getPerson()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='departmentInFaculty'"
 *        annotation="xhttp://www.eclipse.org/emf/2002/Ecore/OCL/Pivot departmentInFaculty='faculty.departments-&gt;includes(affiliation)'"
 * @generated
 */
public interface Person extends Named {
	/**
	 * Returns the value of the '<em><b>Affiliation</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Affiliation</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Affiliation</em>' reference.
	 * @see #setAffiliation(Department)
	 * @see tdt4250.hal.univ.UnivPackage#getPerson_Affiliation()
	 * @model
	 * @generated
	 */
	Department getAffiliation();

	/**
	 * Sets the value of the '{@link tdt4250.hal.univ.Person#getAffiliation <em>Affiliation</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Affiliation</em>' reference.
	 * @see #getAffiliation()
	 * @generated
	 */
	void setAffiliation(Department value);

	/**
	 * Returns the value of the '<em><b>Faculty</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link tdt4250.hal.univ.Faculty#getStaff <em>Staff</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Faculty</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Faculty</em>' container reference.
	 * @see #setFaculty(Faculty)
	 * @see tdt4250.hal.univ.UnivPackage#getPerson_Faculty()
	 * @see tdt4250.hal.univ.Faculty#getStaff
	 * @model opposite="staff" transient="false"
	 * @generated
	 */
	Faculty getFaculty();

	/**
	 * Sets the value of the '{@link tdt4250.hal.univ.Person#getFaculty <em>Faculty</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Faculty</em>' container reference.
	 * @see #getFaculty()
	 * @generated
	 */
	void setFaculty(Faculty value);

} // Person
