/**
 */
package tdt4250.hal.univ.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;

import tdt4250.hal.univ.Department;
import tdt4250.hal.univ.Faculty;
import tdt4250.hal.univ.Person;
import tdt4250.hal.univ.UnivPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Person</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link tdt4250.hal.univ.impl.PersonImpl#getAffiliation <em>Affiliation</em>}</li>
 *   <li>{@link tdt4250.hal.univ.impl.PersonImpl#getFaculty <em>Faculty</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PersonImpl extends NamedImpl implements Person {
	/**
	 * The cached value of the '{@link #getAffiliation() <em>Affiliation</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAffiliation()
	 * @generated
	 * @ordered
	 */
	protected Department affiliation;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PersonImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return UnivPackage.Literals.PERSON;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Department getAffiliation() {
		if (affiliation != null && affiliation.eIsProxy()) {
			InternalEObject oldAffiliation = (InternalEObject)affiliation;
			affiliation = (Department)eResolveProxy(oldAffiliation);
			if (affiliation != oldAffiliation) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, UnivPackage.PERSON__AFFILIATION, oldAffiliation, affiliation));
			}
		}
		return affiliation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Department basicGetAffiliation() {
		return affiliation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAffiliation(Department newAffiliation) {
		Department oldAffiliation = affiliation;
		affiliation = newAffiliation;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UnivPackage.PERSON__AFFILIATION, oldAffiliation, affiliation));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Faculty getFaculty() {
		if (eContainerFeatureID() != UnivPackage.PERSON__FACULTY) return null;
		return (Faculty)eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFaculty(Faculty newFaculty, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newFaculty, UnivPackage.PERSON__FACULTY, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFaculty(Faculty newFaculty) {
		if (newFaculty != eInternalContainer() || (eContainerFeatureID() != UnivPackage.PERSON__FACULTY && newFaculty != null)) {
			if (EcoreUtil.isAncestor(this, newFaculty))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newFaculty != null)
				msgs = ((InternalEObject)newFaculty).eInverseAdd(this, UnivPackage.FACULTY__STAFF, Faculty.class, msgs);
			msgs = basicSetFaculty(newFaculty, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UnivPackage.PERSON__FACULTY, newFaculty, newFaculty));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case UnivPackage.PERSON__FACULTY:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetFaculty((Faculty)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case UnivPackage.PERSON__FACULTY:
				return basicSetFaculty(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case UnivPackage.PERSON__FACULTY:
				return eInternalContainer().eInverseRemove(this, UnivPackage.FACULTY__STAFF, Faculty.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case UnivPackage.PERSON__AFFILIATION:
				if (resolve) return getAffiliation();
				return basicGetAffiliation();
			case UnivPackage.PERSON__FACULTY:
				return getFaculty();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case UnivPackage.PERSON__AFFILIATION:
				setAffiliation((Department)newValue);
				return;
			case UnivPackage.PERSON__FACULTY:
				setFaculty((Faculty)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case UnivPackage.PERSON__AFFILIATION:
				setAffiliation((Department)null);
				return;
			case UnivPackage.PERSON__FACULTY:
				setFaculty((Faculty)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case UnivPackage.PERSON__AFFILIATION:
				return affiliation != null;
			case UnivPackage.PERSON__FACULTY:
				return getFaculty() != null;
		}
		return super.eIsSet(featureID);
	}

} //PersonImpl
