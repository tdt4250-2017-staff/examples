/**
 */
package tdt4250.hal.univ.impl;

import org.eclipse.emf.ecore.EClass;

import tdt4250.hal.univ.Department;
import tdt4250.hal.univ.UnivPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Department</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class DepartmentImpl extends NamedImpl implements Department {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DepartmentImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return UnivPackage.Literals.DEPARTMENT;
	}

} //DepartmentImpl
