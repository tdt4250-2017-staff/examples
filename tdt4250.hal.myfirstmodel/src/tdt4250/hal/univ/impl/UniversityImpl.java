/**
 */
package tdt4250.hal.univ.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.ECollections;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import tdt4250.hal.univ.Faculty;
import tdt4250.hal.univ.Person;
import tdt4250.hal.univ.UnivPackage;
import tdt4250.hal.univ.University;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>University</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link tdt4250.hal.univ.impl.UniversityImpl#getFaculties <em>Faculties</em>}</li>
 *   <li>{@link tdt4250.hal.univ.impl.UniversityImpl#getAllEmployees <em>All Employees</em>}</li>
 * </ul>
 *
 * @generated
 */
public class UniversityImpl extends NamedImpl implements University {
	/**
	 * The cached value of the '{@link #getFaculties() <em>Faculties</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFaculties()
	 * @generated
	 * @ordered
	 */
	protected EList<Faculty> faculties;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UniversityImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return UnivPackage.Literals.UNIVERSITY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Faculty> getFaculties() {
		if (faculties == null) {
			faculties = new EObjectContainmentEList<Faculty>(Faculty.class, this, UnivPackage.UNIVERSITY__FACULTIES);
		}
		return faculties;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<Person> getAllEmployees() {
		List<Person> employees = new ArrayList<>();
		for (Faculty faculty : getFaculties()) {
			employees.addAll(faculty.getStaff());
		}
		return ECollections.unmodifiableEList(employees);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean isEmployed(Person person) {
		return getAllEmployees().contains(person);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case UnivPackage.UNIVERSITY__FACULTIES:
				return ((InternalEList<?>)getFaculties()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case UnivPackage.UNIVERSITY__FACULTIES:
				return getFaculties();
			case UnivPackage.UNIVERSITY__ALL_EMPLOYEES:
				return getAllEmployees();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case UnivPackage.UNIVERSITY__FACULTIES:
				getFaculties().clear();
				getFaculties().addAll((Collection<? extends Faculty>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case UnivPackage.UNIVERSITY__FACULTIES:
				getFaculties().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case UnivPackage.UNIVERSITY__FACULTIES:
				return faculties != null && !faculties.isEmpty();
			case UnivPackage.UNIVERSITY__ALL_EMPLOYEES:
				return !getAllEmployees().isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case UnivPackage.UNIVERSITY___IS_EMPLOYED__PERSON:
				return isEmployed((Person)arguments.get(0));
		}
		return super.eInvoke(operationID, arguments);
	}

} //UniversityImpl
