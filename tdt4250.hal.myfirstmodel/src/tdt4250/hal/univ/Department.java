/**
 */
package tdt4250.hal.univ;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Department</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see tdt4250.hal.univ.UnivPackage#getDepartment()
 * @model
 * @generated
 */
public interface Department extends Named {
} // Department
