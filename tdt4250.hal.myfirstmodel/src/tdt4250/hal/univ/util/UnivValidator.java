/**
 */
package tdt4250.hal.univ.util;

import java.util.Map;

import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.EObjectValidator;

import tdt4250.hal.univ.*;

/**
 * <!-- begin-user-doc -->
 * The <b>Validator</b> for the model.
 * <!-- end-user-doc -->
 * @see tdt4250.hal.univ.UnivPackage
 * @generated
 */
public class UnivValidator extends EObjectValidator {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final UnivValidator INSTANCE = new UnivValidator();

	/**
	 * A constant for the {@link org.eclipse.emf.common.util.Diagnostic#getSource() source} of diagnostic {@link org.eclipse.emf.common.util.Diagnostic#getCode() codes} from this package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.common.util.Diagnostic#getSource()
	 * @see org.eclipse.emf.common.util.Diagnostic#getCode()
	 * @generated
	 */
	public static final String DIAGNOSTIC_SOURCE = "tdt4250.hal.univ";

	/**
	 * A constant with a fixed name that can be used as the base value for additional hand written constants.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final int GENERATED_DIAGNOSTIC_CODE_COUNT = 0;

	/**
	 * A constant with a fixed name that can be used as the base value for additional hand written constants in a derived class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final int DIAGNOSTIC_CODE_COUNT = GENERATED_DIAGNOSTIC_CODE_COUNT;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UnivValidator() {
		super();
	}

	/**
	 * Returns the package of this validator switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EPackage getEPackage() {
	  return UnivPackage.eINSTANCE;
	}

	/**
	 * Calls <code>validateXXX</code> for the corresponding classifier of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected boolean validate(int classifierID, Object value, DiagnosticChain diagnostics, Map<Object, Object> context) {
		switch (classifierID) {
			case UnivPackage.UNIVERSITY:
				return validateUniversity((University)value, diagnostics, context);
			case UnivPackage.NAMED:
				return validateNamed((Named)value, diagnostics, context);
			case UnivPackage.FACULTY:
				return validateFaculty((Faculty)value, diagnostics, context);
			case UnivPackage.DEPARTMENT:
				return validateDepartment((Department)value, diagnostics, context);
			case UnivPackage.PERSON:
				return validatePerson((Person)value, diagnostics, context);
			default:
				return true;
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateUniversity(University university, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(university, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(university, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(university, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(university, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(university, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(university, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(university, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(university, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(university, diagnostics, context);
		if (result || diagnostics != null) result &= validateNamed_nameLongerThanShortName(university, diagnostics, context);
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateNamed(Named named, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(named, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(named, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(named, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(named, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(named, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(named, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(named, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(named, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(named, diagnostics, context);
		if (result || diagnostics != null) result &= validateNamed_nameLongerThanShortName(named, diagnostics, context);
		return result;
	}

	/**
	 * Validates the nameLongerThanShortName constraint of '<em>Named</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean validateNamed_nameLongerThanShortName(Named named, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (chechNameShorterThanShortName(named)) {
			if (diagnostics != null) {
				diagnostics.add
					(createDiagnostic
						(Diagnostic.ERROR,
						 DIAGNOSTIC_SOURCE,
						 0,
						 "_UI_GenericConstraint_diagnostic",
						 new Object[] { "nameLongerThanShortName", getObjectLabel(named, context) },
						 new Object[] { named },
						 context));
			}
			return false;
		}
		return true;
	}

	private boolean chechNameShorterThanShortName(Named named) {
		return named.getName() != null && named.getShortName() != null && named.getName().length() <= named.getShortName().length();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateFaculty(Faculty faculty, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(faculty, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(faculty, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(faculty, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(faculty, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(faculty, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(faculty, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(faculty, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(faculty, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(faculty, diagnostics, context);
		if (result || diagnostics != null) result &= validateNamed_nameLongerThanShortName(faculty, diagnostics, context);
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDepartment(Department department, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(department, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(department, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(department, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(department, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(department, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(department, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(department, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(department, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(department, diagnostics, context);
		if (result || diagnostics != null) result &= validateNamed_nameLongerThanShortName(department, diagnostics, context);
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validatePerson(Person person, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(person, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(person, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(person, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(person, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(person, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(person, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(person, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(person, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(person, diagnostics, context);
		if (result || diagnostics != null) result &= validateNamed_nameLongerThanShortName(person, diagnostics, context);
		if (result || diagnostics != null) result &= validatePerson_departmentInFaculty(person, diagnostics, context);
		return result;
	}

	/**
	 * Validates the departmentInFaculty constraint of '<em>Person</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validatePerson_departmentInFaculty(Person person, DiagnosticChain diagnostics, Map<Object, Object> context) {
		// TODO implement the constraint
		// -> specify the condition that violates the constraint
		// -> verify the diagnostic details, including severity, code, and message
		// Ensure that you remove @generated or mark it @generated NOT
		if (false) {
			if (diagnostics != null) {
				diagnostics.add
					(createDiagnostic
						(Diagnostic.ERROR,
						 DIAGNOSTIC_SOURCE,
						 0,
						 "_UI_GenericConstraint_diagnostic",
						 new Object[] { "departmentInFaculty", getObjectLabel(person, context) },
						 new Object[] { person },
						 context));
			}
			return false;
		}
		return true;
	}

	/**
	 * Returns the resource locator that will be used to fetch messages for this validator's diagnostics.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		// TODO
		// Specialize this to return a resource locator for messages specific to this validator.
		// Ensure that you remove @generated or mark it @generated NOT
		return super.getResourceLocator();
	}

} //UnivValidator
