/**
 */
package tdt4250.hal.univ.util;

import org.eclipse.emf.common.util.URI;

import org.eclipse.emf.ecore.xmi.impl.XMIResourceImpl;

/**
 * <!-- begin-user-doc -->
 * The <b>Resource </b> associated with the package.
 * <!-- end-user-doc -->
 * @see tdt4250.hal.univ.util.UnivResourceFactoryImpl
 * @generated
 */
public class UnivResourceImpl extends XMIResourceImpl {
	/**
	 * Creates an instance of the resource.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param uri the URI of the new resource.
	 * @generated
	 */
	public UnivResourceImpl(URI uri) {
		super(uri);
	}

} //UnivResourceImpl
