/**
 */
package tdt4250.hal.univ;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Faculty</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link tdt4250.hal.univ.Faculty#getDepartments <em>Departments</em>}</li>
 *   <li>{@link tdt4250.hal.univ.Faculty#getStaff <em>Staff</em>}</li>
 * </ul>
 *
 * @see tdt4250.hal.univ.UnivPackage#getFaculty()
 * @model
 * @generated
 */
public interface Faculty extends Named {
	/**
	 * Returns the value of the '<em><b>Departments</b></em>' containment reference list.
	 * The list contents are of type {@link tdt4250.hal.univ.Department}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Departments</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Departments</em>' containment reference list.
	 * @see tdt4250.hal.univ.UnivPackage#getFaculty_Departments()
	 * @model containment="true"
	 * @generated
	 */
	EList<Department> getDepartments();

	/**
	 * Returns the value of the '<em><b>Staff</b></em>' containment reference list.
	 * The list contents are of type {@link tdt4250.hal.univ.Person}.
	 * It is bidirectional and its opposite is '{@link tdt4250.hal.univ.Person#getFaculty <em>Faculty</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Staff</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Staff</em>' containment reference list.
	 * @see tdt4250.hal.univ.UnivPackage#getFaculty_Staff()
	 * @see tdt4250.hal.univ.Person#getFaculty
	 * @model opposite="faculty" containment="true"
	 * @generated
	 */
	EList<Person> getStaff();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model personRequired="true"
	 * @generated
	 */
	void hire(Person person, Department department);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model personRequired="true"
	 * @generated
	 */
	void retire(Person person);

} // Faculty
