/**
 */
package tdt4250.hal.univ;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>University</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link tdt4250.hal.univ.University#getFaculties <em>Faculties</em>}</li>
 *   <li>{@link tdt4250.hal.univ.University#getAllEmployees <em>All Employees</em>}</li>
 * </ul>
 *
 * @see tdt4250.hal.univ.UnivPackage#getUniversity()
 * @model
 * @generated
 */
public interface University extends Named {
	/**
	 * Returns the value of the '<em><b>Faculties</b></em>' containment reference list.
	 * The list contents are of type {@link tdt4250.hal.univ.Faculty}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Faculties</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Faculties</em>' containment reference list.
	 * @see tdt4250.hal.univ.UnivPackage#getUniversity_Faculties()
	 * @model containment="true"
	 * @generated
	 */
	EList<Faculty> getFaculties();

	/**
	 * Returns the value of the '<em><b>All Employees</b></em>' reference list.
	 * The list contents are of type {@link tdt4250.hal.univ.Person}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>All Employees</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>All Employees</em>' reference list.
	 * @see tdt4250.hal.univ.UnivPackage#getUniversity_AllEmployees()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 * @generated
	 */
	EList<Person> getAllEmployees();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model personRequired="true"
	 * @generated
	 */
	boolean isEmployed(Person person);

} // University
